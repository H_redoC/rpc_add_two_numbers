package main

import (
	"fmt"
	"log"
	"net/rpc"

	"github.com/harsh-sheth/local_rpc/server"
)

const serverAddress string = "127.0.0.1"

func main() {
  client, err := rpc.DialHTTP("tcp", serverAddress + ":1234")
  if err != nil {
    log.Fatal("dialing:", err)
  }

  var a, b int
  fmt.Print("Enter two numbers to add via rpc: ")
  fmt.Scanf("%d %d", &a, &b)

  args := &server.Args{A: a, B: b}
  var reply int
  err = client.Call("Arith.Add", args, &reply)
  if err != nil {
    log.Fatal("arith error:", err)
  }
  fmt.Printf("Arith: %d + %d = %d\n", args.A, args.B, reply)

}
