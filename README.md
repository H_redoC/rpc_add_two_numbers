# rpc_add_two_numbers

RPC program to add two numbers

# How to run
clone the repo and cd into this folder.

Run `go run client.go`

# Prerequisites
You should have go install in your system.
